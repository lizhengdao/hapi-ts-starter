import * as Hapi from "@hapi/hapi";

const PORT: number = 3000;
const init = async() => {
    const server = Hapi.server({
        port: 3000,
        host: "localhost"
    });

    server.route({
        method: 'GET',
        path: '/',
        handler: (request, h) => {
            return "Hello World!";
        }
    });

    await server.start();
    console.log(`Server is now listening on port ${PORT}...`);
}

process.on('unhandledRejection', err => {
    console.error(err);
    process.exit(1);
});

init();
